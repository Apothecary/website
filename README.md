# The Apothecary Website

This repo contains the code to build our website.
- Currently located at: [the-apothecary.club](https://the-apothecary.club/).
- Soon to be hosted at: [apothecary.gay](https://apothecary.gay).

## Rules

Our rules are now contained in this repo in the file 
[./content/coc/_index.md](./content/coc/_index.md).