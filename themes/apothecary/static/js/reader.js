const RSS_URL = `https://cathode.church/@apothecary.rss`;

function htmlDecode(input) {
  let doc = new DOMParser().parseFromString(input, "text/html");
  return doc.documentElement.textContent;
}

fetch(RSS_URL)
    .then(resp => resp.text())
    .then(s => new window.DOMParser().parseFromString(s, "text/xml"))
    .then(data => {
        const items = data.querySelectorAll("item");
        let html = ``;
        let numOutputed = 0;
        items.forEach(item => {
            if (numOutputed < 5) {
                html += `
                <article>
                <a href="${item.querySelector("link").innerHTML}" target="_blank" rel="noopener">
                  <h4>${item.querySelector("pubDate").innerHTML}</h4>
                </a>
                <p>${htmlDecode(item.querySelector("description").innerHTML)}</p>
                </article>
                `;
            }
            numOutputed += 1;
        });
        document.getElementById("reader-output").innerHTML = html;
    })
