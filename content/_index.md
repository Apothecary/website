# The Apothecary
is a chill and welcoming left-leaning community, a minority friendly space, a place
of self-expression and empowerment. The community is heavily moderated for the safety and comfort of
our members, if that's not something you're comfortable with, this isn't the space for you. Be
respectful of others, even those you don't understand.

We mainly exist on [Matrix](https://matrix.to/#/#general:the-apothecary.club). Join our [Community
Space](https://matrix.to/#/#community:the-apothecary.club) to see all of our rooms.
A few of our most popular rooms include our 
![LGBTQ+ room logo](lgbt.png) [LGBTQ+](https://matrix.to/#/#LGBTQ:the-apothecary.club),
![Trans room logo](trans.png) [Trans](https://matrix.to/#/#trans:the-apothecary.club) and
![Nerds room logo](nerds.png) [Nerds](https://matrix.to/#/#nerds:the-apothecary.club) rooms.

Be sure to check out our page on [Matrix tips](https://the-apothecary.club/coc/matrix-tricks)
for information on emotes, spoilering, and commands.

We also host two web clients, ![Cinny logo](https://cinny.in/assets/favicon-16x16.png)&nbsp;[Cinny](https://cinny.the-apothecary.club)
and ![SchildiChat icon](https://schildi.chat/img/favicons/favicon-16x16.png)&nbsp;[SchildiChat](https://schildichat.the-apothecary.club).

We're also on Fedi, [@apothecary@cathode.church](https://cathode.church/@apothecary)
(DM to ask for an account on our homeserver), and host our code on [Codeberg account](https://codeberg.org/Apothecary),
including this website and our Matrix bot [Emily](https://codeberg.org/Apothecary/mx-emily).