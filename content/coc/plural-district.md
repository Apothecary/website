---
title: "Plural District Code of Conduct"
date: 2022-11-19T00:02:37-05:00
toc: true
---
# Plural District Code of Conduct

Welcome! To keep this space comfortable for everymany, we have to make a few requests. This document is not exhaustive, and will be modified and expanded over time.

This space welcomes [endogenic](https://bw.artemislena.eu/multiplicity/wiki/Endogenic), [traumagenic](https://bw.artemislena.eu/multiplicity/wiki/Traumagenic), [quoigenic](https://bw.artemislena.eu/multiplicity/wiki/Quoigenic), [neurogenic](https://bw.artemislena.eu/multiplicity/wiki/Neurogenic), [xenogenic](https://bw.artemislena.eu/multiplicity/wiki/Xeno-Origin), [median](https://bw.artemislena.eu/multiplicity/wiki/Median_System), [thoughtforms](https://bw.artemislena.eu/multiplicity/wiki/Thoughtform), mixed-origin, questioning, and other types of plural and non-singlet experiences.

As this space is a safe place for plural groups, singlets are not welcome, with the exception of the Q&A room.

Using more than one account is welcome. Direct message or ask a mod to send an invite to avoid welcome room shenanigans, or simply explain the situation in the welcome room.

Try not to assume second-perspective pronouns (2pp), as they can be different for different plural groups. Using the account display name, or member chat tags is usually safe when unknown.

This is a space to celebrate our Plurality and to let other plural groups celebrate theirs. We do offer support if a plural group or a member of a plural group isn't feeling particularly good, though we ask such vents or anything CW worthy please be put in the appropriate room or spoiler tagged as such ([see below](#spoiler-tags)).

## Community behaviour

Shared Intrasystem Responsibility: it is the responsibility of the entire system or group to act according to the guidelines.

This space seeks to cultivate a safe atmosphere. If others behave in a way that causes discomfort, let them know, or contact a moderator. Similarly, if others voice discomfort, respect that. Harassment will not be tolerated.

While failure to recognize the boundaries of others may be met at first with a gentle attempt to educate, repeated boundary violations will not be tolerated.

When interacting with others, always remember mutual consent.

## "The obvious"

There's nothing interesting about sanism, ableism, homomisia, transmisia, racism, sexism, medicalism, sysmisia, exlusionary mind sets or any other unexamined "default settings" some grow up with. Upholding and normalizing bigoted and oppresive ideas is not welcome. This is not a "free speech zone".

## Spoiler tags

To use content warnings on Element/Schildichat Desktop and Web, use `/spoiler`. Alternatively, `/html regular message text <span data-mx-spoiler>hidden content here!</span>`.

Element and Schildichat on mobile can use the `/spoiler` command to hide an entire message. This does not work in replies.

In FluffyChat, Cinny and nheko it is done by doing this:

`regular message text ||hidden content here||` (note, the | is the Vertical bar or pipe symbol.)

For a more extensive guide, see [Matrix Tips &amp; Tricks](https://the-apothecary.club/coc/matrix-tricks/#spoilers)