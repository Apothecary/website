---
title: "Apothecary Code of Conduct"
date: 2022-11-19T00:02:37-05:00
toc: true
---

<!--

Document notes:

Formatting: please try to keep line to around 100 columns long, and wrap after that. This isn't a
hard limit, but should be generally respected.

-->

## Introduction

The Apothecary is a chill and welcoming left-leaning community, a minority friendly space, a place
of self-expression and empowerment. The community is heavily moderated for the safety and comfort of
our members, if that's not something you're comfortable with, this isn't the space for you. Be
respectful of others, even those you don't understand.


## Content

There are specific and strict content policies on this server for the protection of our members.


### Guidelines

- Engage with others in good faith (especially in the heavy-topics rooms)
    - Try to understand the others' points of view and don't just attack people you disagree with
    - Disagreements happen, but try not to escalate the situation and increase hostility
    - If someone wants to disengage from a conversation, let them
    - If someone is espousing truly harmful views or refuses to engage in good faith, call the moderators to deal with them
- Respect boundaries as placed by others
- Make sure a person consents to receiving a DM from you, either by seeing that they have an
  equivalent of "DMs open" in their avatar/name or by asking them directly in a public room.
  (Exception for DMing a mod over time sensitive issues)
- Be respectful/apologize/give space as necessary if someone tells you, or implies through their
  actions, that you crossed a boundary, as best as you understand
- Spoiler content that may be unpleasant, triggering, or when requested. 
  [How to spoiler](https://the-apothecary.club/coc/matrix-tricks/#spoilers).
- Keep discussion topical to the room it's in and migrate elsewhere if needed
- If links to any 'walled garden' site i.e. those requiring a login is shared then either a
  screenshot or text of the relevant content should be shared along with the link.


### Moderation Requests

If you feel moderation action is needed, at any time you can type `!mods` in any official Apothecary
room to call the moderators.


## Disallowed conduct

- Discrimination, including but not limited to:
    - Racism (this includes cultural appropriation, such as appropriation of AAVE/Black English,
      please see: [The AAVE Glossary](https://aaveglossary.carrd.co) for examples of this and an
      explanation of why it shouldn't be used by non black folks.)
    - Antisemitism
    - Islamomisia
    - Sexism
    - Homomisia
    - Transmisia
    - Acemisia
    - Aromisia
    - Bimisia
    - Panmisia
    - Ableism (please see: [The Ableist Language list](https://autistichoya.com/p/ableist-words-and-terms-to-avoid.html) 
    ([Archive.org backup](https://web.archive.org/web/20230913190028/https://autistichoya.com/p/ableist-words-and-terms-to-avoid.html))
    for examples of this and alternatives to common ableist words and phrases.)
    - Sysmisia; tulpas and other members of plural groups must treated as equal to others.
    - Classism
- General exclusionary behavior
- Stalking, harassment, dogpiling, and any other behavior intended to intimidate or provoke others
- Conduct promoting Nazism, fascism, xenomisia, white supremacy, and any related authoritarian or
  right-wing ideology
- Genocide, apartheid, or segregation apologia, including showing support for, or promoting states
  that engage in any of these
- Using terms and slang associated with the alt-right / chan / fascism
- Aggregating, posting, and/or disseminating a person's demographic, personal, or private data
  without express permission (informally called doxxing)
- Illegal pornographic material and any analogous illustrated material (for instance, content
  depicting minors)
- Lewd content in rooms that are not explicitly marked Lewd
- Gratuitous violence or otherwise shocking content
- Overly combative or otherwise disruptive behavior
- Promoting scams, crypto currencies, and large dataset machine learning
- Inviting bots into Apothecary rooms without consent of the admins
- Having profile nicks, pics, or mxid that contain disallowed conduct
- `/ignore`ing moderators and other helpers.
- Any content or conduct that is illegal in Germany or United States


## Politics

Political topics and discussions should be kept to the 
[Politics room](https://matrix.to/#/#politics:the-apothecary.club).
What counts as a political topic is up to moderator discretion.


## Mental Health

Any content which mentions heavy mental health topics, such as suicide or self harm, must be kept
to the heavy-topics rooms and spoilered appropriately.


## Action

Moderators may choose how to deal with unacceptable behaviour. Here is a non-exhaustive list of
things they may do:

- Inform the offender that their actions were unacceptable and should not be repeated.
- Temporarily restrict the offender's ability to send messages.
- Temporarily or permanently ban the offender from the service.

There is currently no official system to appeal moderation decisions but one is in the works. This
document will be updated when that system is ready.


## Scope

These community guidelines apply to all rooms and spaces related to the the-apothecary.club
homeserver and related services.

## Community Rooms

All of the rooms within the community rooms subspace are moderated by members of our community and
not by apothercary mods directly. It's not required that those rooms follow our rules exactly, but
they should still be moderated well to provide a safe atmosphere for our members, or they will be
removed.
