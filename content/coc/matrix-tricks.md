---
title: "Matrix Tips & Tricks"
date: 2022-12-01T23:44:55-05:00
toc: true
---
# Spoilers
are very useful for posting potentially triggering content safely.
Instructions for using spoilers depend on your client.

## FluffyChat, Cinny, Nheko
Spoiler text by adding two pipes to the start and end of the message (no spaces between).

```
[CW: Content warning here.] ||Hidden content here!||
```

## Element/SchildiChat
```
/spoiler Spoilered Text
```
Note that `/`commands don't work in replies on Element/SC Android.

For inline spoilers (to have the CW be in the same message), you can use the `/html ` command on desktop/web or use `[]()` in its place on Element/SC.

```html
/html [CW: Content warning here.] <span data-mx-spoiler>Hidden content here!</span>
```

## Element iOS / Element X
No known ways to make spoilers exist on these clients, consider a [different one](#client-support).

## Image spoilers
⚠️ Clients behave wildly inconsistently when it comes to displaying inline images, not every client is capable of spoilering them.

Post with [Spoilerinator](https://codeberg.org/cf/spoilerinator) to automatically generalte a semi spoilered image embed, or use
[Emily](https://codeberg.org/Apothecary/mx-emily) to help you do that automatically. Use `!sp` in a protected room to find out more.

## Spoiler reasons
⚠️ Not all clients support viewing spoiler reasons, do not use them. This section is for the sake of documentation only.
### Element, SchildiChat, Nheko
Add a value to the `data-mx-spoiler` attribute.
```html
<span data-mx-spoiler="CW: Unpleasant topic.">Hidden content here!</span>
```

### Nheko, FluffyChat
Inside the two double pipes, add another pipe.
```
||CW: Unpleasant topic.|Hidden content here!||
```

# Emotes

Apothecary rooms have emotes scattered amongst them which fit the theme of each room! In all the
rooms you're joined in, check to see if there are emotes available in it!

## Client support
| Client                                  | Platform | Viewing  | Reacts  | Sending  | Uploading  | Short description |
|-----------------------------------------|:--------:|:--------:|:-------:|:--------:|:----------:|-------------------| 
| [FluffyChat](https://fluffychat.im)     | 📱, 🖥¹  |    ✅    |    ✅   |    ✅    |     ✅     | Focused client with clean UI. |
| [SchildiChat](https://schildi.chat)     | 📱², 🖥  |    ✅    |    ❌   |    ✅³   |     ❌     | [We host it!](https://schildichat.the-apothecary.club) Element fork with a lot of QoL features. Has its own theme but can be reverted to Element's or customized otherwise. |
| [Cinny](https://cinny.in)               | 🖥       |    ✅    |    ✅   |    ✅    |     ✅     | [We host it!](https://cinny.the-apothecary.club) Sleek client with a an efficient apporoach of presenting rooms and spaces. |
| [Nheko](https://nheko-reborn.github.io) | 🖥       |    ✅    |    ✅   |    ✅    |     ✅     | Client with some nifty advanced functionality. |
| [Fractal](https://gitlab.gnome.org/World/fractal#fractal) | 🖥🐧 | ❌ | ❌ |  ❌   |     ❌     | GTK + Rust based lightweight client. |
| Element                                 | 📱, 🖥   |    ✅    |    ❌   |    ❌    |     ❌     | [We do not endorse](https://chaos.social/@scy/110321162229302299) using this client. |
| Element X                               | 📱       |    ❌    |    ❌   |    ❌    |     ❌     | [We do not endorse](https://chaos.social/@scy/110321162229302299) using this client. |
| ❔                                      |          |          |         |          |            | Your favorite client is missing from this chart? Functionality changed? [Tell us in `Suggestions`](https://matrix.to/#/#suggestions:the-apothecary.club)! |

---
¹ Web/desktop version available but UI is mobile optimized.

² Android only.

³ Emotes added to the account only appear as a suggestion in the textbox when typing `:`, followed by the emote name.

## Freeform text reacts
FluffyChat: long press to reply to a message, then use the `/react` command.

SchildiChat: open the emoji picker on a message, and type any text in the search bar and
press `React with "[text]"`.

# Miscellaneous
## Commands
### Emily
`@emily:the-apothecary.club` has command(s) for any account to use. 
To have her post automated image spoilers, send the message `!sp` in one of her rooms to have Emily
tell you how to use it.

Emily is the best bot ever and she loves when you tell her how good she is.
## HTML
### `[WIP]`
